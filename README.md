#
## Chương trình cộng 2 chuỗi số
* Thực hiện bởi: Nguyễn Quốc Lưu
* Email: quocluu2k1@gmail.com
* Ngôn ngữ sử dụng: Java
## Cách biên dịch và chạy chương trình:(Đã clone project về máy và đã có môi trường chạy java)
#### Cách 1:
- Vào đường dẫn "\target\classes" của project và mở command lên.
- Sau đó, chạy lệnh "java Main".
#### Cách 2:
- Mở IDE Java lên và Import project vào. (Khuyến nghị sử dụng IDE IntelliJ)
- Sau đó, chạy chương trình(Chạy hàm main) và chạy các Test Case
