import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyBigNumberTest {
    MyBigNumber myBigNumber = new MyBigNumber();

    @Test
    void test1(){
        assertEquals(myBigNumber.sum("1234","567"),"1801");
    }
    @Test
    void test2(){
        assertEquals(myBigNumber.sum("9999","1"),"10000");
    }
    @Test
    void test3(){
        assertEquals(myBigNumber.sum("9999","9999"),"19998");
    }
    @Test
    void test4(){
        assertEquals(myBigNumber.sum("88","119"),"207");
    }
    @Test
    void test5(){
        assertEquals(myBigNumber.sum("17453242432","5368234382"),"22821476814");
    }
}
