import java.util.Scanner;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhap vao chuoi so thu nhat: ");
        String strNum1 = scanner.nextLine();
        System.out.print("Nhap vao chuoi so thu hai: ");
        String strNum2 = scanner.nextLine();

        System.out.println("\n\nCac buoc thuc hien: \n");

        MyBigNumber myBigNumber = new MyBigNumber();
        String resultSum = myBigNumber.sum(strNum1, strNum2);

        LOGGER.info("Vay, Tong cua hai so " + strNum1 + " va " + strNum2 + " la " + resultSum);
    }
}
