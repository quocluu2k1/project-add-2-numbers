import java.util.logging.Logger;

public class MyBigNumber {
    public String sum(String stn1, String stn2){
        StringBuilder resultSum = new StringBuilder();;
        int maxLeng;
        int temp=0;
        int leng1 = stn1.length();
        int leng2 = stn2.length();

        if(leng1>leng2){
            maxLeng = stn1.length();
        }else{
            maxLeng = stn2.length();
        }
        int i;
        for(i=0;i<maxLeng;i++){
            char c1, c2;
            int num1, num2;

            if(i>=leng1){
                c1 = '0';
                c2 = stn2.charAt(leng2-i-1);
            }else if(i>=leng2){
                c1 =  stn1.charAt(leng1-i-1);
                c2 = '0';
            }else{
                c1 =  stn1.charAt(leng1-i-1);
                c2 = stn2.charAt(leng2-i-1);
            }

            System.out.print("Buoc "+(i+1)+": ");

            num1 = (int)c1-48;
            num2 = (int)c2-48;

            if(((int)c1 + (int)c2 + temp)>=106){
                resultSum.insert(0,(char)(((int)c1 + (int)c2+ temp)-58));

                System.out.print("Lay " + num1 + " cong voi " + num2 + " duoc " + (num1+num2) + ".");
                if (temp == 1) {
                    System.out.println(" Cong tiep voi nho 1 duoc " + (num1+num2+temp));
                }else{
                    System.out.println();
                }
                System.out.println("\tLuu " + (num1+num2+temp-10) + " vao ket qua, duoc ket qua moi la " + resultSum.toString() + ".");
                System.out.println("\tGhi nho 1.");

                temp=1;
            }else{
                resultSum.insert(0,(char)(((int)c1 + (int)c2 + temp)-48));

                System.out.print("Lay " + num1 + " cong voi "+ num2 +" duoc " + (num1+num2) + ".");
                if (temp == 1) {
                    System.out.println(" Cong tiep voi nho 1 duoc " + (num1+num2+temp));
                }else{
                    System.out.println();
                }
                System.out.println("\tLuu " + (num1+num2+temp) + " vao ket qua, duoc ket qua moi la " + resultSum.toString() + ".");

                temp=0;
            }
        }

        if(temp==1){
            resultSum.insert(0, '1');
            System.out.print("Buoc " + (i+1) + ": ");
            System.out.println("Lay nho 1 cong voi 0 duoc 1.");
            System.out.println("\tLuu 1 vao ket qua, duoc ket qua moi la " + resultSum.toString() + ".");
        }

        return resultSum.toString();
    }
}
